import json
from mykafka import MyKafka

def follow(syslog_file):
    pubsub = MyKafka(["localhost:9092"])
    line = syslog_file
    if not line:
        print("no log!")
    else:
        pubsub.send_page_data(syslog_file, 'test1')
with open('./mylog.txt') as f:
    follow(f.read())