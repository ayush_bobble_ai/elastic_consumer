FROM python:3

COPY requirements.txt ./
COPY ./kafka_consumer_es.py ./

RUN pip install --no-cache-dir -r requirements.txt
CMD [ "python", "-u", "./kafka_consumer_es.py" ]

