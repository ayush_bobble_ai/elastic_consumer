from elasticsearch import Elasticsearch
from kafka import KafkaConsumer
import json
import sys

es = Elasticsearch(['http://localhost:9200/'])
id=1
while(True):
    cons = KafkaConsumer('test1', bootstrap_servers="localhost:9092")

    for msg in cons:
        entry = msg.value.decode()
        entry = entry[1:-1]
        entry = list(entry.split('\\n'))
        for i in range(len(entry)):
            entry[i] = json.loads('"'+entry[i]+'"')
            print(entry[i])
            json_body = entry[i]
            es.index(index='data', doc_type='testing', id=i, body=json_body)
            id+=1